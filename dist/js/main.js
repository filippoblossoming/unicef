var $body,
	windowHeight,
	windowWidth,
	documentHeight,
	isDesktop = false,
	isMobileDevice = false,
	$menuTrigger,
	$window,
	$progressBar,
	$scrollTop = 0,
	$header = $('.header'),
	$heroTop = $('.hero_top'),
	degree = 0.0174532925,
	mediaPoint1 = 1024,
	mediaPoint2 = 768,
	mediaPoint3 = 480,
	mediaPoint4 = 320;

function onReadyPage() {
	$body = $('body');
	$menuTrigger = $('.menuTrigger');
	$header = $('.header');
	$window = $(window);
	$mainContent = $('.js-main');
	$progressBar = $('.progressBar');

	$('.nav_item_title').on('click', function() {
		var item = $(this).attr('data-tab');
		$('.active_tab').removeClass('active_tab');
		$('.graph_tabs_content_item[data-tab="' + item + '"]').addClass('active_tab');
		$(this).addClass('active_tab');
	});

	$('.btnMenuClose').on('click', function(e) {
		$body.removeClass('menu_open');
		$menuTrigger.removeClass('active_mod');
	});

	$('.scrollToLink').on('click', function(e) {
		e.preventDefault();
		href = $(this).attr('href');
		topY = $(href).offset().top;
		TweenMax.to($window, 1, {
			scrollTo:{
				y: 0,
				offsetY: 0,
				autoKill: false
			},
			ease:Power3.easeOut
		});
		return false;
	})

	$('.scrollHeroLink').on('click', function(e) {
		e.preventDefault();

		var $this = $(this),
				href = $this.attr('href'),
				topY = $(href).offset().top;
				url = document.location.href;

		TweenMax.to($window, 1, {
			scrollTo:{
				y: topY,
				offsetY: $header.height(),
				autoKill: false
			},
			ease:Power3.easeOut
		});

		return false;
	})
}


$(document).ready(function ($) {
	onReadyPage();

	$('body').on('click', function(e) {
		var $this = $(e.target);

		if ($this.hasClass('menuTrigger')) {
			if ($body.hasClass('menu_open')) {
				$body.removeClass('menu_open');
				$this.removeClass('active_mod');
			} else {
				$body.addClass('menu_open');
				$this.addClass('active_mod');
				$('.article_list_item').eq(0).find('.article_block_in').focus();
			}
		}

	});

});

$(window).on('load', function () {
	updateSizes();
	loadFunc();
});

$(window).on('resize', function () {
	resizeFunc();
});

// $(window).on('scroll', function () {
// 	scrollFunc();
// });

$(window).on('scroll', $.throttle(16, scrollFunc));

function loadFunc() {
	calcViewportHeight();
	// barbaFunc();
	tickerFunction();
}

function tickerFunction() {
	if ($heroTop.length) {
		if (windowWidth > mediaPoint1 && !isDesktop) {
			isDesktop = true;
			isMobileDevice = false;

			if (isDesktop) {
				TweenLite.ticker.addEventListener("tick", progressBar);
			}

		} else if (windowWidth <= mediaPoint1 && !isMobileDevice) {
			isDesktop = false;
			isMobileDevice = true;

			if (isMobileDevice) {
				TweenMax.ticker.removeEventListener("tick", progressBar);
			}

		}
	}
}

function resizeFunc() {
	updateSizes();

	calcViewportHeight();
	tickerFunction();
}

function scrollFunc() {
	$scrollTop = $(window).scrollTop();

	//headerScroll();
}

function progressBar() {
	var scrollPercent = ($scrollTop) / (documentHeight - windowHeight);
	var scrollPercentRounded = Math.round(scrollPercent*100);

	TweenMax.to($progressBar, .1, {xPercent: scrollPercentRounded, ease: Linear.easeNone});
}

function barbaFunc() {
	barba.hooks.enter((data) => {

		var currentPage = window.location.pathname.split('/').slice(-1)[0]

		if (currentPage !== 'index.html' && currentPage !== '/') {
			$body.addClass('inner_mod');
		} else {
			$body.removeClass('inner_mod');
		}

	});

	/// Barba
	barba.init({

		transitions: [{
			appear(data) {
				var done = this.async();
				let tl = new TimelineMax({});
				tl
					.from($('.wrapper'), .6, {
						autoAlpha: 0,
						onComplete: done,
					});
			},

			before(data) { // Хук выполняется перед всеми действиями
				if ($body.hasClass('menu_open')) {
					$body.removeClass('menu_open');
					$menuTrigger.removeClass('active_mod');
				}
			},

			after(data) { // Хук выполняется после всех действий

			},

			leave(data) { // Хук ухода со страницы
				var done = this.async();

				TweenMax
					.to(window, .6, {
						scrollTo: {
							y: 0
						},
						onComplete: function() {
						}
					})
				TweenMax
					.to(data.current.container, .6, {
						y: -windowHeight,
						opacity: 0,
						onComplete: done,
					}, '-=.6');
			},
			enter(data) { // Хук входа на  страницу
				var done = this.async();
				TweenMax.fromTo(data.next.container, .6, {
					y: windowHeight,
					opacity: 0
				}, {
					y: 0,
					opacity: 1,
					onComplete: function() {
						onReadyPage();
						done();
					}
				});

			},
		}, ],
	});
}

// function headerScroll() {
// 	if ($scrollTop > 10 && !$header.hasClass('scroll_mod')) {
// 		$header.addClass('scroll_mod');
// 		$heroTop.addClass('scroll_mod');
// 	} else if ($scrollTop < 10) {
// 		$header.removeClass('scroll_mod');
// 		$heroTop.removeClass('scroll_mod');
// 	}
// }

function calcViewportHeight() {
	if (isMobile.apple.phone || isMobile.android.phone || isMobile.seven_inch) {
		var vh = window.innerHeight * 0.01;
		// var vh2 = document.documentElement.clientHeight * 0.01;

		document.documentElement.style.setProperty('--vh', vh + 'px');
	}
}

function updateSizes() {
	windowWidth = window.innerWidth;
	windowHeight = window.innerHeight;
	documentHeight = document.body.scrollHeight;
}

if ('objectFit' in document.documentElement.style === false) {
	document.addEventListener('DOMContentLoaded', function () {
		Array.prototype.forEach.call(document.querySelectorAll('img[data-object-fit]'), function (image) {
			(image.runtimeStyle || image.style).background = 'url("' + image.src + '") no-repeat 50%/' + (image.currentStyle ? image.currentStyle['object-fit'] : image.getAttribute('data-object-fit'));

			image.src = 'data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'' + image.width + '\' height=\'' + image.height + '\'%3E%3C/svg%3E';
		});
	});
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

function getRandom(min, max) {
	return Math.random() * (max - min) + min;
}

const styles = ['color: #fff', 'background: #000000'].join(';');

const message = 'Developed by Blossoming https://blossoming.it/';

console.info('%c%s', styles, message);
